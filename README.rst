flowstub
########

This little vim plugin identifies Javascript files containing flow annotations
(by detecting the /*** @flow ***/ header) and then makes adjustments that make
life easier (disables the JSHint plugin which can't grok Flow annotations, and
enables the flow checker in Syntastic, which shouldn't otherwise have it
enabled for Javascript files, to be run in addition to whatever other checkers
you are having Syntastic run. Check out the source - that's all this does.

If you don't use Vim, or you don't use Flow, or if you don't use either JSHint
or Syntastic, then this has no use to you.
