function! s:flow_stub_check()
    let l:line = getline(1)
    if l:line =~# '^/\* @flow \*/$'
        " Let other plugins know this is both Javascript and Flow
        set ft=javascript.flow

        " JSHint cannot cope with Flow's type annotations.
        let b:jshint_disabled = 1

        " flow shouldn't normally be enabled for Javascript in general,
        " but it should be included for this file.
        if exists('g:syntastic_javascript_checkers')
            let b:syntastic_checkers = copy(g:syntastic_javascript_checkers)
        else
            let b:syntastic_checkers = []
        endif
        call insert(b:syntastic_checkers, 'flow')

        " We probably want results from other checkers presented alongside flow
        " results, since flow only checks the types.
        let b:syntastic_aggregate_errors = 1
    endif
endfunction

autocmd BufNewFile,BufRead *.js call s:flow_stub_check()
